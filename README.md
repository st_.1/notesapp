# A note taking app
## Features
* A note can contain:
    * formating, hyperlinks, ~~coordinates,~~ pictures ~~or other
 files~~
 	* based on `SpannableStringBuilder`
* note storage in SQLite
* images partly stored in internal storage
* XML import/export
* settings saved locally (shared preferences)
* ~~synchronization with a custom server (written in Rust)~~
* ~~android intent filter: "Save as a new note"~~
