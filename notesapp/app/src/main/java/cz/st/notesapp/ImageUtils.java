package cz.st.notesapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageUtils {
    public static Bitmap loadImageWithRotation(Context context, Uri uri) throws IOException {
        Bitmap image;

        // load image from URI
        image = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);

        // fix image orientation (EXIF data)
        int orient;

        ExifInterface exifInterface = new ExifInterface(context.getContentResolver().openInputStream(uri));
        orient = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        Matrix transMatrix = new Matrix();

        switch (orient) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                transMatrix.postRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                transMatrix.postRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                transMatrix.postRotate(270);
                break;
        }

        if (orient != ExifInterface.ORIENTATION_NORMAL)
            image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), transMatrix, true);

        return image;
    }

    public static Bitmap byteArrayToBitmap(byte[] byteArray) {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    public static byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 95, byteStream);
        return byteStream.toByteArray();
    }
}
