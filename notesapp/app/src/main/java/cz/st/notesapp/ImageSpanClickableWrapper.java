package cz.st.notesapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

// a wrapper span, used to make ImageSpan clickable
public class ImageSpanClickableWrapper extends ClickableSpan {
    long id;
    String filename;
    String mime;
    Uri uri;

    public ImageSpanClickableWrapper(Context context, Uri uri) {
        this.id = -1; // isn't saved in db
        this.filename = Utils.uriToFilename(context, uri);
        this.mime = context.getContentResolver().getType(uri);
        this.uri = uri;
    }

    public ImageSpanClickableWrapper(Context context, long id, String filename, String mime) {
        this.id = id; // saved in db
        this.filename = filename;
        this.mime = mime;
    }

    @Override
    public void onClick(@NonNull View view) {
        Context context = view.getContext();
        Intent imageViewActivityIntent = new Intent(context, ImageViewActivity.class);
        imageViewActivityIntent.putExtra("id", this.id);
        imageViewActivityIntent.putExtra("filename", this.filename);
        imageViewActivityIntent.putExtra("uri", this.uri); // null if id > 0
        context.startActivity(imageViewActivityIntent);
    }

    @Override
    public void updateDrawState (TextPaint ds) {
        ds.setUnderlineText(false);
        ds.setStrokeWidth(0);
    }
}
