package cz.st.notesapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.text.style.URLSpan;
import android.view.View;

import java.util.regex.Pattern;

import androidx.annotation.NonNull;

public class ClickableURLSpan extends URLSpan {
    private Context context;

    public ClickableURLSpan(String url, Context context) {
        super(url);
        this.context = context;
    }

    public ClickableURLSpan(@NonNull Parcel src) {
        super(src);
    }

    @Override
    public void onClick(View widget){
        // setup onclick function for the URLSpan
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(fixUrl(this.getURL())));
        context.startActivity(intent);
    }

    private String fixUrl(String url) {
        if (Pattern.matches("^[a-zA-Z]*://.*", url))
            return url;

        // prepend url with https:// if a protocol is missing
        return "https://" + url;
    }
}
