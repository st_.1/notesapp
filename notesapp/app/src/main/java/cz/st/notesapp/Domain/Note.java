package cz.st.notesapp.Domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.TimeZone;

import cz.st.notesapp.DatabaseHelper;
import cz.st.notesapp.Utils;

public class Note implements Serializable {
    long id;
    String name;
    String text;
    LocalDateTime created;
    LocalDateTime lastChange;
    ArrayList<Style> styles;       // bold, underline, italic, strikethrough, link
    ArrayList<Image> images;       // images, thumbnails
    ArrayList<FileStore> fileStores;         // unused
    ArrayList<Location> locations; // unused

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getCreated() {
        return this.created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastChange() {
        return this.lastChange;
    }

    public void setLastChange(LocalDateTime lastChange) {
        this.lastChange = lastChange;
    }

    public ArrayList<Style> getStyles() {
        if (this.styles == null)
            lazyLoad();

        return this.styles;
    }

    public void setStyles(ArrayList<Style> styles) { this.styles = styles; }

    public ArrayList<Image> getImages() {
        if (this.images == null)
            lazyLoad();

        return this.images;
    }

    public void setImages(ArrayList<Image> images) { this.images = images; }

    public ArrayList<FileStore> getFileStores() {
        if (this.fileStores == null)
            lazyLoad();

        return this.fileStores;
    }

    public void setFileStores(ArrayList<FileStore> fileStores) { this.fileStores = fileStores; }

    public ArrayList<Location> getLocations() {
        if (this.locations == null)
            lazyLoad();

        return this.locations;
    }

    public void setLocations(ArrayList<Location> locations) { this.locations = locations; }

    // don't load dependencies until they're actually needed
    private void lazyLoad() {
        this.styles = Style.findByNoteId(id);
        this.images = Image.findByNoteId(id);
        this.fileStores = cz.st.notesapp.Domain.FileStore.findByNoteId(id);
        this.locations = Location.findByNoteId(id);
    }

    public String getPerex(int noLines) {
        String[] lines = this.text.split("\n");
        String perex = "";

        for (int i = 0; i < noLines; i++)
            if (lines.length > i) {
                if (i > 0)
                    perex += "\n";
                perex += lines[i];
            }

        return perex;
    }

    public Note(long id, String name, String text, LocalDateTime created,
                LocalDateTime lastChange) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.created = created;
        this.lastChange = lastChange;
        this.styles = styles;
        this.images = images;
        this.fileStores = fileStores;
        this.locations = locations;
    }

    public Note(String name, String text,
                ArrayList<Style> styles, ArrayList<Image> images,
                ArrayList<FileStore> fileStores, ArrayList<Location> locations) {
        this.name = name;
        this.text = text;
        this.created = this.lastChange = LocalDateTime.now();
        this.styles = styles;
        this.images = images;
        this.fileStores = fileStores;
        this.locations = locations;
    }

    public Note(String name, String text) {
        this.name = name;
        this.text = text;
        this.created = this.lastChange = LocalDateTime.now();
        this.styles = null;
        this.images = null;
        this.fileStores = null;
        this.locations = null;
    }

    public static ArrayList<Note> find() {
        return find("");
    }

    public static ArrayList<Note> find(String keyword) {
        ArrayList<Note> notes = new ArrayList<Note>();

        SQLiteDatabase db = DatabaseHelper.getDatabase();

        Cursor res;

        if (keyword.trim().isEmpty())
            res = db.rawQuery("SELECT * FROM note", null);
        else
            res = db.rawQuery("SELECT * FROM note WHERE name LIKE ? OR text LIKE ?",
                    new String[] { "%" + keyword + "%", "%" + keyword + "%" });

        res.moveToFirst();

        while (res.isAfterLast() == false) {
            long id = res.getLong(res.getColumnIndex("id"));
            String name = res.getString(res.getColumnIndex("name"));
            String text = res.getString(res.getColumnIndex("text"));
            long createdTimestamp = res.getLong(res.getColumnIndex("created"));
            long lastChangeTimestamp = res.getLong(res.getColumnIndex("last_change"));
            LocalDateTime created = LocalDateTime.ofInstant(Instant.ofEpochMilli(createdTimestamp), TimeZone.getDefault().toZoneId());
            LocalDateTime lastChange = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastChangeTimestamp), TimeZone.getDefault().toZoneId());

            notes.add(new Note(id, name, text, created, lastChange));
            res.moveToNext();
        }

        return notes;
    }

    public static Note findById(long id) {

        SQLiteDatabase db = DatabaseHelper.getDatabase();

        Cursor res = db.rawQuery("SELECT * FROM note WHERE id = ?", new String[] { String.valueOf(id) });

        id = res.getInt(res.getColumnIndex("id"));
        String name = res.getString(res.getColumnIndex("name"));
        String text = res.getString(res.getColumnIndex("text"));
        long createdTimestamp = res.getLong(res.getColumnIndex("created"));
        long lastChangeTimestamp = res.getLong(res.getColumnIndex("last_change"));
        LocalDateTime created = LocalDateTime.ofInstant(Instant.ofEpochMilli(createdTimestamp), TimeZone.getDefault().toZoneId());
        LocalDateTime lastChange = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastChangeTimestamp), TimeZone.getDefault().toZoneId());

        ArrayList<Style> styles = Style.findByNoteId(id);
        ArrayList<Image> images = Image.findByNoteId(id);
        ArrayList<FileStore> fileStores = cz.st.notesapp.Domain.FileStore.findByNoteId(id);
        ArrayList<Location> locations = Location.findByNoteId(id);

        return new Note(id, name, text, created, lastChange);
    }

    public static long insert(String name, String text, ArrayList<Style> styles, ArrayList<Image> images, Context context) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("name", name);
        contentValues.put("text", text);
        contentValues.put("created", System.currentTimeMillis());
        contentValues.put("last_change", System.currentTimeMillis());

        // note id on success, -1 on failure
        long id = db.insert("note", null, contentValues);

        if (id <= 0)
            return id;

        for (Style style : styles)
            style.insert(id);

        for (Image image : images)
            image.insert(id, context);

        return id;
    }

    public boolean delete(Context context) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        int deletedRows = db.delete("note", "id = ?", new String[] { String.valueOf(id) });

        for (Style style : this.styles)
            style.delete();

        for (Image image : this.images)
            image.delete(context);

        return deletedRows > 0;
    }

    public boolean update(ArrayList<Style> styles, ArrayList<Image> images, Context context) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("name", name);
        contentValues.put("text", text);
        contentValues.put("last_change", System.currentTimeMillis());

        // delete old formatting
        for (Style style : this.styles)
            style.delete();

        for (Image image : this.images)
            image.delete(context);

        // insert new formatting
        this.styles = styles;
        this.images = images;

        for (Style style : this.styles)
            style.insert(this.id);

        for (Image image : this.images)
            image.insert(this.id, context);

        return db.update("note", contentValues, "id = ?", new String[] {String.valueOf(this.id)}) > 0;
    }

    public static int clear() {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        return db.delete("style", null, null);
    }

    public static long insertFromXml(long id, String name, String text, LocalDateTime created, LocalDateTime lastChange) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("id", id);
        contentValues.put("name", name);
        contentValues.put("text", text);
        contentValues.put("created", Utils.dateTimeToTimestamp(created));
        contentValues.put("last_change", Utils.dateTimeToTimestamp(lastChange));

        // note id on success, -1 on failure
        return db.insert("note", null, contentValues);
    }
}