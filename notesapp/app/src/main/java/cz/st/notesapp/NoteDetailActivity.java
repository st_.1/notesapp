package cz.st.notesapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import cz.st.notesapp.Domain.Image;
import cz.st.notesapp.Domain.Note;
import cz.st.notesapp.Domain.Style;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.AlignmentSpan;
import android.text.style.CharacterStyle;
import android.text.style.ImageSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.Size;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class NoteDetailActivity extends AppCompatActivity {

    LinearLayout bottomToolbar;
    EditText noteNameEditText;
    EditText noteTextEditText;
    TextView noteNameTextView;
    TextView noteTextTextView;
    ViewSwitcher viewEditSwitcher;

    ImageButton boldButton;
    ImageButton italicButton;
    ImageButton underlineButton;
    ImageButton strikethroughButton;
    ImageButton insertLinkButton;
    ImageButton insertImageButton;
    ImageButton insertAttachmentButton;

    Note note = null;
    boolean readOnly = true;

    final int INSERT_IMAGE = 0;
    final int INSERT_FILE = 1;
    final int INSERT_LOCATION = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        // toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("New Note");

        bottomToolbar = findViewById(R.id.bottomToolbar);
        noteNameEditText = findViewById(R.id.editTextNoteName);
        noteTextEditText = findViewById(R.id.editTextNoteText);
        noteNameTextView = findViewById(R.id.textViewNoteName);
        noteTextTextView = findViewById(R.id.textViewNoteText);
        viewEditSwitcher = findViewById(R.id.viewEditSwitcher);

        boldButton = findViewById(R.id.boldButton);
        italicButton = findViewById(R.id.italicButton);
        underlineButton = findViewById(R.id.underlineButton);
        strikethroughButton = findViewById(R.id.strikethroughButton);
        insertLinkButton = findViewById(R.id.insertLinkButton);
        insertImageButton = findViewById(R.id.insertImageButton);
        insertAttachmentButton = findViewById(R.id.insertAttachmentButton);

        registerForContextMenu(insertAttachmentButton);

        // enable hyperlinks
        noteTextTextView.setMovementMethod(LinkMovementMethod.getInstance());

        Intent intent = getIntent();
        if (intent != null) {
            note = (Note) intent.getSerializableExtra("note");
            readOnly = intent.getBooleanExtra("readOnly", true);

            if (note != null) {

                // load title & text

                noteNameEditText.setText(note.getName());

                SpannableStringBuilder ssb = new SpannableStringBuilder(note.getText());
                noteTextEditText.setText(ssb);

                // load formatting (Image, Style entries)

                loadFormatting(note);

                getSupportActionBar().setTitle(note.getName().isEmpty() ? "Unnamed Note" : "Note: " + note.getName());
            }
        }

        if (readOnly)
            switchViews(readOnly);

        // keyboard open/close detection
        // https://stackoverflow.com/a/4737265

        final View noteDetailRoot = findViewById(R.id.noteDetailRoot);
        noteDetailRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // calculate height difference in order to detect if a keyboard is opened
                int heightDiff = noteDetailRoot.getRootView().getHeight() - noteDetailRoot.getHeight();

                if (heightDiff > Utils.dpToPx(noteDetailRoot.getContext(), 200)) {
                    // keyboard is opened -> show formatting bar
                    bottomToolbar.setVisibility(View.VISIBLE);
                } else {
                    // keyboard is closed -> hide formatting bar
                    bottomToolbar.setVisibility(View.GONE);
                }
            }
        });

        // disable formatting bar unless noteText is selected
        noteTextEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                setBottomToolbarEnable(b);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        getMenuInflater().inflate(R.menu.insert_attach_menu, menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_note_detail, menu);

        if (note == null)
            menu.findItem(R.id.icon_remove_note).setVisible(false);

        if (readOnly)
            menu.findItem(R.id.icon_save_note).setVisible(false);
        else
            menu.findItem(R.id.icon_edit_note).setVisible(false);

        return true;
    }

    private void applyStyleEntry(Style style) {
        switch (style.getType()) {
            case BOLD:
                applySpan(new StyleSpan(Typeface.BOLD), style.getBegin(), style.getEnd());
                break;
            case ITALIC:
                applySpan(new StyleSpan(Typeface.ITALIC), style.getBegin(), style.getEnd());
                break;
            case UNDERLINE:
                applySpan(new UnderlineSpan(), style.getBegin(), style.getEnd());
                break;
            case STRIKETROUGH:
                applySpan(new StrikethroughSpan(), style.getBegin(), style.getEnd());
                break;
            case LINK:
                applySpan(new ClickableURLSpan(style.getValue(), this), style.getBegin(), style.getEnd());
                break;
        }
    }

    private void applyImageEntry(Image image) {
        applySpanNonText(
                image.getTextPos(),
                image.getTextPos() + 1,
                new ImageSpan(
                    this,
                    image.getThumbnail()),
                true
        );
        applySpanNonText(
                image.getTextPos(),
                image.getTextPos() + 1,
                new ImageSpanClickableWrapper(
                    this,
                    image.getId(),
                    image.getFilename(),
                    image.getMime()
                ),
                true
        );
    }

    private void loadFormatting(Note note) {
        ArrayList<Style> styles = note.getStyles();
        ArrayList<Image> images = note.getImages();

        if (styles == null || images == null) {
            Toast.makeText(this, "Unable to load formatting", Toast.LENGTH_SHORT).show();
            return;
        }

        for (Style style : styles)
            applyStyleEntry(style);

        for (Image image : images)
            applyImageEntry(image);
    }

    // converts spans to Style entries
    private ArrayList<Style> generateStyleArrayFromSpans(SpannableStringBuilder ssb, Object spans[]) {
        ArrayList<Style> styles = new ArrayList<Style>();

        for (Object span : spans) {
            int begin = ssb.getSpanStart(span);
            int end = ssb.getSpanEnd(span);

            if (span instanceof StyleSpan) {
                StyleSpan styleSpan = (StyleSpan) span;

                if (styleSpan.getStyle() == Typeface.BOLD)
                    styles.add(new Style(begin, end, TextStyling.BOLD, null));
                else if (styleSpan.getStyle() == Typeface.ITALIC)
                    styles.add(new Style(begin, end, TextStyling.ITALIC, null));

            } else if (span instanceof UnderlineSpan) {
                styles.add(new Style(begin, end, TextStyling.UNDERLINE, null));
            } else if (span instanceof StrikethroughSpan) {
                styles.add(new Style(begin, end, TextStyling.STRIKETROUGH, null));
            } else if (span instanceof URLSpan) {
                URLSpan urlSpan = (URLSpan) span;
                styles.add(new Style(begin, end, TextStyling.LINK, urlSpan.getURL()));
            }
        }

        return styles;
    }

    // converts spans to Image entries
    private ArrayList<Image> generateImageArrayFromSpans(SpannableStringBuilder ssb, Object spans[]) throws IOException {
        ArrayList<Image> images = new ArrayList<Image>();

        // get all wrappers & imageSpans
        ArrayList<ImageSpanClickableWrapper> wrappers = new ArrayList<ImageSpanClickableWrapper>();
        ArrayList<ImageSpan> imageSpans = new ArrayList<ImageSpan>();

        for (Object span : spans)
            if (span instanceof ImageSpanClickableWrapper)
                wrappers.add((ImageSpanClickableWrapper) span);
            else if (span instanceof ImageSpan)
                imageSpans.add((ImageSpan) span);

        // build image array
        for (ImageSpanClickableWrapper wrapper : wrappers)
        {
            ImageSpan imageSpan;

            // find corresponding image span
            for (ImageSpan span : imageSpans)
                if (ssb.getSpanStart(span) == ssb.getSpanStart(wrapper)) {
                    imageSpan = span;

                    Bitmap imageBitmap;

                    if (wrapper.uri != null)
                        imageBitmap = ImageUtils.loadImageWithRotation(this, wrapper.uri);
                    else
                        imageBitmap = Image.find(wrapper.id).getImage(this);

                    // make an image object out of the matched pair,
                    // add it to the array
                    images.add(new Image(
                                    wrapper.id,
                                    wrapper.filename,
                                    wrapper.mime,
                                    imageBitmap,
                                    ((BitmapDrawable) imageSpan.getDrawable()).getBitmap(),
                                    false,
                                    ssb.getSpanStart(wrapper)
                    ));
                    break;
                }
        }

        return images;
    }

    public void saveNote(MenuItem item) {
        // prevent saving an empty note
        if (noteNameEditText.getText().toString().trim().isEmpty() &&
            noteTextEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "The note is empty.", Toast.LENGTH_SHORT).show();
            return;
        }

        // convert spans to databate entries
        SpannableStringBuilder ssb = (SpannableStringBuilder) noteTextEditText.getText();
        Object spans[] = ssb.getSpans(0, ssb.length(), Object.class);

        ArrayList<Style> styles;
        ArrayList<Image> images;

        try {
            styles = generateStyleArrayFromSpans(ssb, spans);
            images = generateImageArrayFromSpans(ssb, spans);
        } catch(Exception e) {
            Toast.makeText(this, "Unable to save note, failed to generate formatting data", Toast.LENGTH_SHORT).show();
            return;
        }

        if (note == null) {
            // insert
            long id = Note.insert(
                    noteNameEditText.getText().toString(),
                    noteTextEditText.getText().toString(),
                    styles,
                    images,
                    this
            );

            if (id <= 0)
                Toast.makeText(this, "Failed to save note.", Toast.LENGTH_SHORT).show();
            else
                switchViews(true);
        } else {
            // update
            note.setName(noteNameEditText.getText().toString());
            note.setText(noteTextEditText.getText().toString());

            boolean updated = note.update(styles, images, this);

            if (!updated)
                Toast.makeText(this, "Failed to update note.", Toast.LENGTH_SHORT).show();
            else
                switchViews(true);
        }
    }

    private void removeNote() {
        boolean deleted = note.delete(this);

        if (!deleted)
            Toast.makeText(this, "Failed to remove note.", Toast.LENGTH_SHORT).show();
        else
            onBackPressed();
    }

    public void editNote(MenuItem item) {
        switchViews(false);
    }

    // handles switching between read only and edit mode
    public void switchViews(boolean readOnly) {
        this.readOnly = readOnly;
        invalidateOptionsMenu();

        if (readOnly) {
            noteNameTextView.setText(noteNameEditText.getText());
            noteTextTextView.setText(noteTextEditText.getText());
            Utils.hideKeyboard(this);
        } else {
            noteNameEditText.setText(noteNameTextView.getText());
            noteTextEditText.setText(noteTextTextView.getText());
        }

        viewEditSwitcher.showNext();
        findViewById(R.id.noteDetailRoot).clearFocus();
    }

    public void removeNoteConfirmationDialog(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(note.getName().isEmpty() ? "Delete Unnamed Note" : "Delete Note: " + note.getName());
        builder.setMessage("Are you sure you want to delete this note?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                removeNote();
                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create().show();
    }

    private void setBottomToolbarEnable(boolean b) {
        boldButton.setEnabled(b);
        italicButton.setEnabled(b);
        underlineButton.setEnabled(b);
        strikethroughButton.setEnabled(b);
        insertLinkButton.setEnabled(b);
        insertImageButton.setEnabled(b);
        insertAttachmentButton.setEnabled(b);
    }

    public void applySpan(CharacterStyle style) {
        applySpan(style,
            noteTextEditText.getSelectionStart(),
            noteTextEditText.getSelectionEnd());
    }

    // a helper function for Span object filtration
    private ArrayList<Object> getFilteredSpanRange(SpannableStringBuilder ssb, CharacterStyle style, int begin, int end) {
        ArrayList<Object> existingSpans = new ArrayList<Object>(Arrays.asList(ssb.getSpans(begin, end, style.getClass())));

        Iterator<Object> it = existingSpans.iterator();
        while(it.hasNext()){
            Object span = it.next();

            // make sure we distinguish bold from italic as they're the same class
            if (span instanceof StyleSpan && style instanceof StyleSpan) {
                StyleSpan styleSpanApplied = (StyleSpan) style;
                StyleSpan styleSpanLoop = (StyleSpan) span;

                if (styleSpanApplied.getStyle() != styleSpanLoop.getStyle()) {
                    it.remove();
                    continue;
                }
            }
        }

        return existingSpans;
    }

    private boolean spanRangeDeletionMark(CharacterStyle style) {
        SpannableStringBuilder ssb = (SpannableStringBuilder) noteTextEditText.getText();
        int begin = noteTextEditText.getSelectionStart();
        int end = noteTextEditText.getSelectionEnd();
        ArrayList<Object> existingSpans = getFilteredSpanRange(ssb, style, begin, end);

        return spanRangeDeletionMark(ssb, existingSpans, style, begin, end);
    }

    private boolean spanRangeDeletionMark(SpannableStringBuilder ssb,
                                          ArrayList<Object> existingSpans,
                                          CharacterStyle style,
                                          int begin, int end) {

        Iterator<Object> it = existingSpans.iterator();
        while(it.hasNext()){
            Object span = it.next();

            // determine if spans should be deleted
            if (span instanceof CharacterStyle) {
                int spanBegin = ssb.getSpanStart(span);
                int spanEnd = ssb.getSpanEnd(span);

                if (spanBegin <= begin && spanEnd >= end) {
                    return true;
                }
            }
        }

        return false;
    }

    private void applySpan(CharacterStyle style, int begin, int end) {
        // get current ssb
        SpannableStringBuilder ssb = (SpannableStringBuilder) noteTextEditText.getText();

        // get existing spans of the same kind
        ArrayList<Object> existingSpans = getFilteredSpanRange(ssb, style, begin, end);

        // determine if we should delete existing spans or add a new one
        boolean markSpansForDeletion = spanRangeDeletionMark(ssb, existingSpans, style, begin, end);

        // delete existing spans if appropriate
        if (markSpansForDeletion)
            for (Object span : existingSpans)
                ssb.removeSpan(span);
        else
            // otherwise create a new one
            ssb.setSpan(style, begin, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // update EditText with new ssb
        int selStart = noteTextEditText.getSelectionStart();
        int selEnd = noteTextEditText.getSelectionEnd();
        noteTextEditText.setText(ssb);
        noteTextEditText.setSelection(selStart, selEnd);
    }

    private void applySpanNonText(CharacterStyle span) {
        applySpanNonText(span, false);
    }

    private void applySpanNonText(CharacterStyle span, boolean noChar) {
        int selStart = noteTextEditText.getSelectionStart();
        int selEnd = noteTextEditText.getSelectionEnd();

        applySpanNonText(selStart, selEnd, span, noChar);
    }

    private void applySpanNonText(int selStart, int selEnd, CharacterStyle span, boolean noChar) {
        // get current ssb, selection
        SpannableStringBuilder ssb = (SpannableStringBuilder) noteTextEditText.getText();

        // as we need a piece of text to display a span we'll replace the selection with '-
        if (!noChar)
            ssb.replace(selStart, selEnd,"-");
        ssb.setSpan(span, selStart, selStart + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        if (span instanceof ImageSpanClickableWrapper)
            ssb.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
                    selStart, selStart + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // update EditText with new ssb
        noteTextEditText.setText(ssb);
        noteTextEditText.setSelection(selStart, selEnd);
    }

    public void formatBold(View view) {
        applySpan(new StyleSpan(Typeface.BOLD));
    }

    public void formatItalic(View view) {
        applySpan(new StyleSpan(Typeface.ITALIC));
    }

    public void formatUnderline(View view) {
        applySpan(new UnderlineSpan());
    }

    public void formatStrikethrough(View view) {
        applySpan(new StrikethroughSpan());
    }

    public void formatURL(View view) {
        CharSequence selectionText = noteTextEditText.getText().subSequence(
            noteTextEditText.getSelectionStart(),
            noteTextEditText.getSelectionEnd()
        );

        if (selectionText.length() == 0)
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("URL");
        builder.setMessage("Enter a URL for this link");

        EditText urlEditText = new EditText(this);
        urlEditText.setText(selectionText);
        builder.setView(urlEditText);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                applySpan(new ClickableURLSpan(urlEditText.getText().toString(), NoteDetailActivity.this));
                dialogInterface.dismiss();
            }
        });

        // only bring up dialog if we're not going to delete the current URL span
        if (spanRangeDeletionMark(new URLSpan("")) == false)
            builder.create().show();
        else
            applySpan(new URLSpan(""));
    }

    public void insertImage(View view) {
        int curPos = noteTextEditText.getSelectionEnd();

        // request image through an intent object
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, INSERT_IMAGE);
    }

    public void insertFile(MenuItem item) {
        Toast.makeText(this, "Not implemented", Toast.LENGTH_SHORT).show();
        return;

        //int curPos = noteTextEditText.getSelectionEnd();

        // request a file through an intent object
        //Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        //intent.setType("*/*");
        //startActivityForResult(intent, INSERT_FILE);
    }

    public void insertLocation(MenuItem item) {
        Toast.makeText(this, "Not implemented", Toast.LENGTH_SHORT).show();
        return;

        // don't insert unless the selection is empty
        //if (noteTextEditText.getSelectionStart() != noteTextEditText.getSelectionEnd())
        //    return;

        //int curPos = noteTextEditText.getSelectionEnd();
    }

    public void openInsertAttachmentMenu(View view) {
        view.showContextMenu(0,0);
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        InputStream inputStream = null;

        if (requestCode == INSERT_IMAGE) {
            Bitmap image;
            Bitmap thumbnail = data.getParcelableExtra("data");
            Uri uri = data.getData();

            // create thumbnail if it doesn't exist
            if (thumbnail == null)
            {
                try {
                    image = ImageUtils.loadImageWithRotation(this, uri);
                } catch (IOException e) {
                    Toast.makeText(this, "Unable to load image", Toast.LENGTH_SHORT).show();
                    return;
                }

                Size displaySize = Utils.getDisplaySize(this);

                double ratio = displaySize.getWidth() / (double) Integer.max(image.getWidth(), image.getHeight())  * 0.8;

                thumbnail = ThumbnailUtils.extractThumbnail(
                        image,
                        (int)(image.getWidth() * ratio),
                        (int)(image.getHeight() * ratio)
                );
            }

            // apply resulting span + a clickable wrapper
            applySpanNonText(new ImageSpan(this, thumbnail));
            applySpanNonText(new ImageSpanClickableWrapper(this, uri), true);
        }

        if (requestCode == INSERT_FILE) {
            Toast.makeText(this, "Not implemented", Toast.LENGTH_SHORT).show();
        }

        if (requestCode == INSERT_LOCATION) {
            Toast.makeText(this, "Not implemented", Toast.LENGTH_SHORT).show();
        }
    }
}