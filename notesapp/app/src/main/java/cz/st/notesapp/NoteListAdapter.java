package cz.st.notesapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import cz.st.notesapp.Domain.Note;

public class NoteListAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private ArrayList<Note> notes;
    private int previewNoLines;

    public NoteListAdapter(ArrayList<Note> notes, int previewNoLines, Context context) {
        this.notes = notes;
        this.previewNoLines = previewNoLines;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return notes.size();
    }

    @Override
    public Object getItem(int i) {
        return notes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View noteRow = layoutInflater.inflate(R.layout.note_row, viewGroup, false);

        TextView nameTextView = noteRow.findViewById(R.id.nameTextView);
        TextView perexTextView = noteRow.findViewById(R.id.perexTextView);
        TextView lastChangeTextView = noteRow.findViewById(R.id.lastChangeTextView);
        nameTextView.setText(notes.get(i).getName());
        perexTextView.setText(notes.get(i).getPerex(previewNoLines));
        perexTextView.setMaxLines(previewNoLines);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        lastChangeTextView.setText(notes.get(i).getLastChange().format(formatter));

        if (notes.get(i).getName().isEmpty()) {
            // hide title
            nameTextView.setVisibility(View.GONE);
        }

        return noteRow;
    }
}