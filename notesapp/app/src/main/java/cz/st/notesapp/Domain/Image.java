package cz.st.notesapp.Domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import cz.st.notesapp.DatabaseHelper;
import cz.st.notesapp.ImageUtils;
import cz.st.notesapp.TextStyling;

public class Image implements Serializable {
    long id;
    long noteId;
    String filename;
    String mime;
    Bitmap image;
    Bitmap thumbnail;
    boolean isAttachment;
    int textPos;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNoteId() { return this.noteId; }

    public String getFilename() { return this.filename; }

    public void setFilename(String filename) { this.filename = filename; }

    public String getMime() { return this.mime; }

    public void setMime(String mime) { this.mime = mime; }

    public Bitmap getImage(Context context) {
        if (this.image == null && id > 0) {
            this.image = loadImageFromInternalStorage(context, this.id, this.filename);
        }

        return this.image;
    }

    public void setImage(Bitmap bitmap) { this.image = image; }

    public Bitmap getThumbnail() {
        if (this.thumbnail == null && id > 0) {
            SQLiteDatabase db = DatabaseHelper.getDatabase();
            Cursor res = db.rawQuery("SELECT thumbnail FROM image WHERE id = ?",
                    new String[] { String.valueOf(id) });
            res.moveToFirst();

            this.thumbnail = ImageUtils.byteArrayToBitmap(res.getBlob(res.getColumnIndex("thumbnail")));
        }

        return this.thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) { this.thumbnail = thumbnail; }

    public boolean getIsAttachment() { return this.isAttachment; }

    public void setIsAttachment(boolean isAttachment) { this.isAttachment = isAttachment; }

    public int getTextPos() { return this.textPos; }

    public void setTextPos(int textPos) { this.textPos = textPos; }

    public Image(long id, String filename, String mime, Bitmap image, Bitmap thumbnail, boolean isAttachment, int textPos) {
        this.id = id;
        this.filename = filename;
        this.mime = mime;
        this.image = image;
        this.thumbnail = thumbnail;
        this.isAttachment = isAttachment;
        this.textPos = textPos;
    }

    private Image(long id, long noteId, String filename, String mime, boolean isAttachment, int textPos) {
        this.id = id;
        this.noteId = noteId;
        this.filename = filename;
        this.mime = mime;
        this.image = null;
        this.thumbnail = null;
        this.isAttachment = isAttachment;
        this.textPos = textPos;
    }

    public Image(long id, String filename, String mime, boolean isAttachment, int textPos) {
        this.id = id;
        this.filename = filename;
        this.mime = mime;
        this.image = null;
        this.thumbnail = null;
        this.isAttachment = isAttachment;
        this.textPos = textPos;
    }

    public static ArrayList<Image> find() {
        ArrayList<Image> images = new ArrayList<Image>();
        SQLiteDatabase db = DatabaseHelper.getDatabase();

        Cursor res = db.rawQuery("SELECT id, id_note, filename, mime, is_attachment, text_pos FROM image", null);

        res.moveToFirst();

        while (res.isAfterLast() == false) {
            long id = res.getLong(res.getColumnIndex("id"));
            long noteId = res.getLong(res.getColumnIndex("id_note"));
            String filename = res.getString(res.getColumnIndex("filename"));
            String mime = res.getString(res.getColumnIndex("mime"));
            boolean isAttachment = res.getInt(res.getColumnIndex("is_attachment")) > 0;
            int textPos = res.getInt(res.getColumnIndex("text_pos"));

            images.add(new Image(id, noteId, filename, mime, isAttachment, textPos));
            res.moveToNext();
        }

        return images;
    }

    public static Image find(long id) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        Cursor res = db.rawQuery("SELECT filename, mime, is_attachment, text_pos FROM image WHERE id = ?",
                new String[] { String.valueOf(id) });

        res.moveToFirst();

        String filename = res.getString(res.getColumnIndex("filename"));
        String mime = res.getString(res.getColumnIndex("mime"));
        boolean isAttachment = res.getInt(res.getColumnIndex("is_attachment")) > 0;
        int textPos = res.getInt(res.getColumnIndex("text_pos"));

        return new Image(id, filename, mime, isAttachment, textPos);
    }

    public static ArrayList<Image> findByNoteId(long noteId) {
        ArrayList<Image> images = new ArrayList<Image>();
        SQLiteDatabase db = DatabaseHelper.getDatabase();

        Cursor res = db.rawQuery("SELECT id, filename, mime, is_attachment, text_pos FROM image WHERE id_note = ?",
                new String[] { String.valueOf(noteId) });

        res.moveToFirst();

        while (res.isAfterLast() == false) {
            long id = res.getLong(res.getColumnIndex("id"));
            String filename = res.getString(res.getColumnIndex("filename"));
            String mime = res.getString(res.getColumnIndex("mime"));
            boolean isAttachment = res.getInt(res.getColumnIndex("is_attachment")) > 0;
            int textPos = res.getInt(res.getColumnIndex("text_pos"));

            images.add(new Image(id, filename, mime, isAttachment, textPos));
            res.moveToNext();
        }

        return images;
    }

    public long insert(long noteId, Context context) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("id_note", noteId);
        contentValues.put("filename", this.filename);
        contentValues.put("mime", this.mime);
        contentValues.put("thumbnail", ImageUtils.bitmapToByteArray(this.thumbnail));
        contentValues.put("is_attachment", this.isAttachment ? 1 : 0);
        contentValues.put("text_pos", this.textPos);

        long id = db.insert("image", null, contentValues);

        if (id > 0)
            try {
                saveImageToInternalStorage(context, this.image, id, this.filename);
            } catch (Exception e) {
                // if saving failed, delete the database record aswell
                if (id > 0)
                    db.delete("image", "id = ?", new String[] { String.valueOf(this.id) });
                return -1;
            }

        // image id on success, -1 on failure
        return id;
    }

    public boolean updateTextPos(long textPos) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("text_pos", textPos);

        return db.update("image", contentValues, "id = ?",
                new String[] {String.valueOf(this.id)}) > 0;
    }

    public boolean delete(Context context) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        int deletedRows = db.delete("image", "id = ?", new String[] { String.valueOf(this.id) });
        deleteImageFromInternalStorage(context, this.id, this.filename);
        return deletedRows > 0;
    }

    // as the CursorWindow simply isn't large enough for bigger photos
    // we'll use application's internal storage instead
    // eg. image_$ID_$FILENAME

    private static void saveImageToInternalStorage(Context context, Bitmap image, long id, String filename) throws IOException {
        FileOutputStream fos = context.openFileOutput("image_" + id + "_" + filename, Context.MODE_PRIVATE);
        fos.write(ImageUtils.bitmapToByteArray(image));
    }

    private Bitmap loadImageFromInternalStorage(Context context, long id, String filename) {
        try (FileInputStream fis = context.openFileInput("image_" + id + "_" + filename)) {
            byte[] byteArray = new byte[fis.available()];
            fis.read(byteArray);
            return ImageUtils.byteArrayToBitmap(byteArray);
        } catch (Exception e) {
            return null;
        }
    }

    private void deleteImageFromInternalStorage(Context context, long id, String filename) {
        File file = new File(context.getFilesDir(), "image_" + id + "_" + filename);
        file.delete();
    }

    public static int clear() {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        return db.delete("style", null, null);
    }

    public static long insertFromXml(long id, long noteId, String filename,
                                     String mime, Bitmap thumbnail, Bitmap image,
                                     boolean isAttachment, int textPos, Context context) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("id", id);
        contentValues.put("id_note", noteId);
        contentValues.put("filename", filename);
        contentValues.put("mime", mime);
        contentValues.put("thumbnail", ImageUtils.bitmapToByteArray(thumbnail));
        contentValues.put("is_attachment", isAttachment ? 1 : 0);
        contentValues.put("text_pos", textPos);

        id = db.insert("image", null, contentValues);

        if (id <= 0)
            return id;

        try {
            saveImageToInternalStorage(context, image, id, filename);
        } catch (Exception e) {
            // if saving failed, delete the database record aswell
            if (id > 0)
                db.delete("image", "id = ?", new String[] { String.valueOf(id) });
            return -1;
        }

        // image id on success, -1 on failure
        return id;
    }
}
