package cz.st.notesapp.Domain;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;


import cz.st.notesapp.DatabaseHelper;
import cz.st.notesapp.TextStyling;

public class Style {
    long id;
    long noteId;
    int begin;
    int end;
    TextStyling type;
    String value;

    public long getId() { return this.id; }

    public void setId(long id) { this.id = id; }

    public long getNoteId() { return this.noteId; }

    public int getBegin() { return this.begin; }

    public void setBegin(int begin) { this.begin = begin; }

    public int getEnd() { return this.end; }

    public void setEnd(int end) { this.end = end; }

    public TextStyling getType() { return this.type; }

    public void setType(TextStyling type) { this.type = type; }

    public String getValue() { return this.value; }

    public void setValue() { this.value = value; }

    public Style() {}

    private Style(long id, long noteId, int begin, int end, TextStyling type, String value) {
        this.id = id;
        this.noteId = noteId;
        this.begin = begin;
        this.end = end;
        this.type = type;
        this.value = value;
    }

    public Style(int begin, int end, TextStyling type, String value) {
        this.begin = begin;
        this.end = end;
        this.type = type;
        this.value = value;
    }

    public static ArrayList<Style> find() {
        ArrayList<Style> styles = new ArrayList<Style>();
        SQLiteDatabase db = DatabaseHelper.getDatabase();

        Cursor res = db.rawQuery("SELECT * FROM style", null);

        res.moveToFirst();

        while (res.isAfterLast() == false) {
            long id = res.getLong(res.getColumnIndex("id"));
            long noteId = res.getLong(res.getColumnIndex("id_note"));
            int begin = res.getInt(res.getColumnIndex("begin"));
            int end = res.getInt(res.getColumnIndex("end"));
            TextStyling type = TextStyling.getEnumValue(res.getInt(res.getColumnIndex("type")));
            String value = res.getString(res.getColumnIndex("value"));

            styles.add(new Style(id, noteId, begin, end, type, value));
            res.moveToNext();
        }

        return styles;
    }

    public static ArrayList<Style> findByNoteId(long noteId) {
        ArrayList<Style> styles = new ArrayList<Style>();
        SQLiteDatabase db = DatabaseHelper.getDatabase();

        Cursor res = db.rawQuery("SELECT * FROM style WHERE id_note = ?", new String[] { String.valueOf(noteId) });

        res.moveToFirst();

        while (res.isAfterLast() == false) {
            long id = res.getLong(res.getColumnIndex("id"));
            int begin = res.getInt(res.getColumnIndex("begin"));
            int end = res.getInt(res.getColumnIndex("end"));
            TextStyling type = TextStyling.getEnumValue(res.getInt(res.getColumnIndex("type")));
            String value = res.getString(res.getColumnIndex("value"));

            styles.add(new Style(id, noteId, begin, end, type, value));
            res.moveToNext();
        }

        return styles;
    }

    public long insert(long noteId) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("id_note", noteId);
        contentValues.put("begin", this.begin);
        contentValues.put("end", this.end);
        contentValues.put("type", this.type.getIntValue());
        contentValues.put("value", this.value);

        // style id on success, -1 on failure
        return db.insert("style", null, contentValues);
    }

    public boolean update() {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("begin", begin);
        contentValues.put("end", end);
        contentValues.put("type", type.getIntValue());
        contentValues.put("value", value);

        return db.update("note", contentValues, "id = ?", new String[] {String.valueOf(this.id)}) > 0;
    }

    public boolean delete() {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        int deletedRows = db.delete("style", "id = ?", new String[] { String.valueOf(this.id) });
        return deletedRows > 0;
    }

    public static int clear() {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        return db.delete("style", null, null);
    }

    public static long insertFromXml(long id, long noteId, int begin, int end, TextStyling type, String value) {
        SQLiteDatabase db = DatabaseHelper.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("id", noteId);
        contentValues.put("id_note", noteId);
        contentValues.put("begin", begin);
        contentValues.put("end", end);
        contentValues.put("type", type.getIntValue());
        contentValues.put("value", value);

        // style id on success, -1 on failure
        return db.insert("style", null, contentValues);
    }
}
