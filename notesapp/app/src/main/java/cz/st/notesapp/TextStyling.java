package cz.st.notesapp;

public enum TextStyling {
    INVALID(-1),
    BOLD(0),
    ITALIC(1),
    UNDERLINE(2),
    STRIKETROUGH(3),
    LINK(4);

    private final int value;

    private TextStyling(int value) {
        this.value = value;
    }

    public int getIntValue() {
        return value;
    }

    public static TextStyling getEnumValue(int value) {
        switch(value) {
            case 0:
                return TextStyling.BOLD;
            case 1:
                return TextStyling.ITALIC;
            case 2:
                return TextStyling.UNDERLINE;
            case 3:
                return TextStyling.STRIKETROUGH;
            case 4:
                return TextStyling.LINK;
        }

        return TextStyling.INVALID;
    }
}
