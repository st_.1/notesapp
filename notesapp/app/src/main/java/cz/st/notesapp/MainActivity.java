package cz.st.notesapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import cz.st.notesapp.Domain.Note;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.style.ImageSpan;
import android.util.Size;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SearchView searchView;
    ImageView searchViewClose;
    ListView noteListView;


    ArrayList<Note> notes;

    final int XML_EXPORT = 0;
    final int XML_IMPORT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // search view

        searchView = findViewById(R.id.searchView);
        searchViewClose = (ImageView) findViewById(R.id.search_close_btn);

        View.OnClickListener searchViewCloseListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setQuery("", true);
                searchView.clearFocus();
                searchView.setVisibility(View.GONE);
                initNoteList();
            }
        };

        SearchView.OnQueryTextListener searchQueryListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                initNoteList();
                return false;
            }
        };

        searchView.setOnQueryTextListener(searchQueryListener);
        searchViewClose.setOnClickListener(searchViewCloseListener);

        // note list

        initNoteList();
    }

    @Override
    public void onPause() {
        super.onPause();

        // hide search view before entering another activity
        searchView.clearFocus();
        searchView.setVisibility(View.GONE);
        searchView.setQuery("", false);
    }

    @Override
    public void onResume() {
        initNoteList();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        // xml export directory selection callback
        if (requestCode == XML_EXPORT) {
            try {
                Uri uri = data.getData();
                DocumentFile directory = DocumentFile.fromTreeUri(this, uri);

                DocumentFile xmlFile = directory.createFile(
                    "text/xml",
                    "notesapp_export_" + LocalDateTime.now().toString()
                );

                OutputStream outputStream = getContentResolver().openOutputStream(xmlFile.getUri());

                XMLAdapter.exportToXML(outputStream, this);
            } catch(Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Failed to export XML", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(this, "Notes were successfully exported", Toast.LENGTH_SHORT).show();
        }

        // xml import file selection callback
        if (requestCode == XML_IMPORT) {
            try {
                Uri uri = data.getData();
                DocumentFile xmlFile = DocumentFile.fromSingleUri(this, uri);

                InputStream inputStream = getContentResolver().openInputStream(xmlFile.getUri());

                XMLAdapter.importFromXml(inputStream, this);
                initNoteList();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Failed to import XML", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(this, "Notes were successfully imported", Toast.LENGTH_SHORT).show();
        }
    }

    public void newNote(MenuItem item) {
        Intent noteDetailActivityIntent = new Intent(this, NoteDetailActivity.class);
        noteDetailActivityIntent.putExtra("readOnly", false);
        startActivity(noteDetailActivityIntent);
    }

    public void openNote(int position) {
        Intent noteDetailActivityIntent = new Intent(this, NoteDetailActivity.class);
        noteDetailActivityIntent.putExtra("note", notes.get(position));
        startActivity(noteDetailActivityIntent);
    }

    public void settings(MenuItem item) {
        Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);
        startActivity(settingsActivityIntent);
    }

    public void searchPopup(MenuItem item) {
        if (searchView.getVisibility() == View.VISIBLE)
        {
            searchView.clearFocus();
            searchView.setVisibility(View.GONE);
        }
        else
        {
            searchView.setVisibility(View.VISIBLE);
            searchView.requestFocusFromTouch();

            Utils.showKeyboard(this);
        }
    }

    public void initNoteList() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int previewNoLines = prefs.getInt("previewNoLines", 3);

        notes = Note.find(searchView.getQuery().toString().trim());

        NoteListAdapter noteListAdapter = new NoteListAdapter(notes, previewNoLines, this);
        noteListView = findViewById(R.id.noteListView);
        noteListView.setAdapter(noteListAdapter);
        noteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                openNote(position);
            }
        });
    }

    public void xmlImport(MenuItem item) {
        Intent chooser = new Intent(Intent.ACTION_GET_CONTENT);
        chooser.addCategory(Intent.CATEGORY_OPENABLE);
        chooser.setType("text/xml");
        Intent intent = Intent.createChooser(chooser, "Choose a XML backup");
        startActivityForResult(intent, XML_IMPORT);
    }

    public void xmlExport(MenuItem item) {
        // in newer android versions writing directly to an sdcard had been blocked
        // however we can use the ACTION_OPEN_DOCUMENT_TREE intent
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        startActivityForResult(intent, XML_EXPORT);
    }
}