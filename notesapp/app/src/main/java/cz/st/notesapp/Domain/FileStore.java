package cz.st.notesapp.Domain;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;

// currently unused

public class FileStore implements Serializable {
    long id;
    String filename;
    String mime;
    byte[] blob;
    boolean isAttachment;
    long textPos;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFilename() { return this.filename; }

    public void setFilename(String filename) { this.filename = filename; }

    public String getMime() { return this.mime; }

    public void setMime(String mime) { this.mime = mime; }

    public byte[] getBlob() { return this.blob; }

    public void setBlob(byte[] blob) { this.blob = blob; }

    public long getTextPos() { return this.textPos; }

    public void setTextPos(long textPos) { this.textPos = textPos; }

    public FileStore() {}

    public FileStore(long id, String filename, String mime, byte[] blob, boolean isAttachment) {
        this.id = id;
        this.filename = filename;
        this.mime = mime;
        this.blob = blob;
        this.isAttachment = isAttachment;
    }

    public FileStore(String filename, String mime, byte[] blob, boolean isAttachment) {
        this.filename = filename;
        this.mime = mime;
        this.blob = blob;
        this.isAttachment = isAttachment;
    }

    public static FileStore find(long id) {
        FileStore fileStore = new FileStore();
        return fileStore;
    }

    public static ArrayList<FileStore> findByNoteId(long noteId) {
        ArrayList<FileStore> fileStores = new ArrayList<FileStore>();
        return fileStores;
    }
}
