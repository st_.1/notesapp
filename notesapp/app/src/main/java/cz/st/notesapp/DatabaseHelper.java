package cz.st.notesapp;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

public class DatabaseHelper {
    private SQLiteDatabase db;

    private DatabaseHelper() {
        db = SQLiteDatabase.openDatabase(
        "/data/data/" + new DummyClass().getClass().getPackage().getName() + "/notesapp.db",
        null,
        SQLiteDatabase.CREATE_IF_NECESSARY | SQLiteDatabase.OPEN_READWRITE
        );

        initDb(db);
    }

    private static DatabaseHelper instance = null;

    private static DatabaseHelper getInstance() {
        if (DatabaseHelper.instance == null)
            DatabaseHelper.instance = new DatabaseHelper();

        return DatabaseHelper.instance;
    }

    private void initDb(SQLiteDatabase db)
    {
        // note
        db.execSQL(
            "CREATE TABLE IF NOT EXISTS note (" +
                "id INTEGER PRIMARY KEY, " +
                "name TEXT, " +
                "text TEXT, " +
                "created INT, " +
                "last_change INT" +
            ")"
        );

        // style
        db.execSQL(
            "CREATE TABLE IF NOT EXISTS style (" +
                "`id` INTEGER PRIMARY KEY, " +
                "`id_note` INTEGER, " +
                "`begin` INTEGER, " +
                "`end` INTEGER, " +
                "`type` INTEGER," +
                "`value` TEXT" +
            ")"
        );

        // image
        db.execSQL(
            "CREATE TABLE IF NOT EXISTS image (" +
                "id INTEGER PRIMARY KEY, " +
                "id_note INTEGER, " +
                "filename TEXT, " +
                "mime TEXT, " +
                "image BLOB," +
                "thumbnail BLOB," +
                "is_attachment INTEGER," +
                "text_pos INTEGER" +
            ")"
        );
    }

    public static SQLiteDatabase getDatabase() {
        return DatabaseHelper.getInstance().db;
    }
}
