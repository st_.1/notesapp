package cz.st.notesapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Base64;
import java.util.TimeZone;

import cz.st.notesapp.Domain.Image;
import cz.st.notesapp.Domain.Note;
import cz.st.notesapp.Domain.Style;

public class XMLAdapter {
    public static void exportToXML(OutputStream outputStream, Context context) throws IOException {
        // get necessary data from domain
        ArrayList<Note> notes = Note.find();
        ArrayList<Style> styles = Style.find();
        ArrayList<Image> images = Image.find();

        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(outputStream, "UTF-8");
        serializer.startDocument(null, true);

        serializer.startTag(null, "notesapp");

        // note entries
        serializer.startTag(null,"notes");

        for (Note note : notes) {
            serializer.startTag(null,"note");

            serializer.startTag(null,"id");
            serializer.text(String.valueOf(note.getId()));
            serializer.endTag(null,"id");

            serializer.startTag(null,"name");
            serializer.text(note.getName());
            serializer.endTag(null,"name");

            serializer.startTag(null,"text");
            serializer.text(note.getText());
            serializer.endTag(null,"text");

            serializer.startTag(null,"created");
            serializer.text(String.valueOf(Utils.dateTimeToTimestamp(note.getCreated())));
            serializer.endTag(null,"created");

            serializer.startTag(null,"last_change");
            serializer.text(String.valueOf(Utils.dateTimeToTimestamp(note.getLastChange())));
            serializer.endTag(null,"last_change");

            serializer.endTag(null, "note");
        }

        serializer.endTag(null, "notes");

        // style entries
        serializer.startTag(null,"styles");

        for (Style style : styles) {
            serializer.startTag(null,"style");

            serializer.startTag(null,"id");
            serializer.text(String.valueOf(style.getId()));
            serializer.endTag(null,"id");

            serializer.startTag(null,"id_note");
            serializer.text(String.valueOf(style.getNoteId()));
            serializer.endTag(null,"id_note");

            serializer.startTag(null,"begin");
            serializer.text(String.valueOf(style.getBegin()));
            serializer.endTag(null,"begin");

            serializer.startTag(null,"end");
            serializer.text(String.valueOf(style.getEnd()));
            serializer.endTag(null,"end");

            serializer.startTag(null,"type");
            serializer.text(String.valueOf(style.getType().getIntValue()));
            serializer.endTag(null,"type");

            serializer.startTag(null,"value");
            serializer.text(String.valueOf(style.getValue()));
            serializer.endTag(null,"value");

            serializer.endTag(null, "style");
        }

        serializer.endTag(null, "styles");

        // image entries
        serializer.startTag(null,"images");

        for (Image image : images) {
            String imageBase64 = "";
            String thumbnailBase64 = "";
            Bitmap imageBitmap = image.getImage(context);
            Bitmap thumbnailBitmap = image.getThumbnail();

            if (imageBitmap != null)
                imageBase64 = Base64.getEncoder().encodeToString(ImageUtils.bitmapToByteArray(imageBitmap));

            if (thumbnailBitmap != null)
                thumbnailBase64 = Base64.getEncoder().encodeToString(ImageUtils.bitmapToByteArray(thumbnailBitmap));

            serializer.startTag(null,"image");

            serializer.startTag(null,"id");
            serializer.text(String.valueOf(image.getId()));
            serializer.endTag(null,"id");

            serializer.startTag(null,"id_note");
            serializer.text(String.valueOf(image.getNoteId()));
            serializer.endTag(null,"id_note");

            serializer.startTag(null,"filename");
            serializer.text(String.valueOf(image.getFilename()));
            serializer.endTag(null,"filename");

            serializer.startTag(null,"_image");
            serializer.text(String.valueOf(imageBase64));
            serializer.endTag(null,"_image");

            serializer.startTag(null,"thumbnail");
            serializer.text(thumbnailBase64);
            serializer.endTag(null,"thumbnail");

            serializer.startTag(null,"is_attachment");
            serializer.text(String.valueOf(image.getIsAttachment()));
            serializer.endTag(null,"is_attachment");

            serializer.startTag(null,"text_pos");
            serializer.text(String.valueOf(image.getTextPos()));
            serializer.endTag(null,"text_pos");

            serializer.endTag(null, "image");
        }

        serializer.endTag(null, "images");

        serializer.endTag(null, "notesapp");

        serializer.endDocument();
    }

    public static void importFromXml(InputStream inputStream, Context context) throws XmlPullParserException, IOException {
        // delete existing data
        Style.clear();
        Image.clear();
        Note.clear();

        // insert data from an xml file
        XmlPullParserFactory xmlFactory = XmlPullParserFactory.newInstance();
        XmlPullParser parser = xmlFactory.newPullParser();

        parser.setInput(inputStream, "UTF-8");

        int event = parser.getEventType();

        // note entry fields
        long note_id = 0;
        String note_name = "";
        String note_text = "";
        LocalDateTime note_created = LocalDateTime.now();
        LocalDateTime note_lastChange = LocalDateTime.now();

        // image entry fields
        long image_id = 0;
        long image_noteId = 0;
        String image_filename = null;
        String image_mime = null;
        Bitmap image_image = null;
        Bitmap image_thumbnail = null;
        boolean image_isAttachment = false;
        int image_textPos = 0;

        // style entry fields
        long style_id = 0;
        long style_noteId = 0;
        int style_begin = 0;
        int style_end = 0;
        TextStyling style_type = TextStyling.INVALID;
        String style_value = null;

        // current entry tag name
        String currentEntry = null;
        String tagName = "";

        while (event != XmlPullParser.END_DOCUMENT) {
            String text = parser.getText();

            if (parser.getName() != null)
                tagName = parser.getName();

            if (event == XmlPullParser.START_TAG) {

                if (tagName.equals("note") || tagName.equals("style") || tagName.equals("image"))
                    currentEntry = tagName;

            }
            else if(event == XmlPullParser.TEXT) {
                if (currentEntry.equals("note")) {
                    switch(tagName) {
                        case "id":
                            note_id = Long.valueOf(text);
                            break;
                        case "name":
                            note_name = text;
                            break;
                        case "text":
                            note_text = text;
                            break;
                        case "created":
                            note_created = Utils.timestampToDateTime(Long.valueOf(text));
                            break;
                        case "last_change":
                            note_lastChange = Utils.timestampToDateTime(Long.valueOf(text));
                            break;
                    }
                } else if(currentEntry.equals("style")) {
                    switch(tagName) {
                        case "id":
                            style_id = Long.valueOf(text);
                            break;
                        case "id_note":
                            style_noteId = Long.valueOf(text);
                            break;
                        case "begin":
                            style_begin = Integer.valueOf(text);
                            break;
                        case "end":
                            style_end = Integer.valueOf(text);
                            break;
                        case "type":
                            style_type = TextStyling.getEnumValue(Integer.valueOf(text));
                            break;
                        case "value":
                            style_value = text;
                            break;
                    }
                } else if(currentEntry.equals("image")) {
                    switch(tagName) {
                        case "id":
                            image_id = Long.valueOf(text);
                            break;
                        case "id_note":
                            image_noteId = Long.valueOf(text);
                            break;
                        case "filename":
                            image_filename = text;
                            break;
                        case "mime":
                            image_mime = text;
                            break;
                        case "thumbnail":
                            image_thumbnail = ImageUtils.byteArrayToBitmap(Base64.getDecoder().decode(text));
                            break;
                        case "_image":
                            image_image = ImageUtils.byteArrayToBitmap(Base64.getDecoder().decode(text));
                            break;
                        case "is_attachment":
                            image_isAttachment = Boolean.valueOf(text);
                            break;
                        case "text_pos":
                            image_textPos = Integer.valueOf(text);
                            break;
                    }
                }
            }
            else if(event == XmlPullParser.END_TAG) {
                // insert entry
                switch (tagName) {
                    case "note":
                        Note.insertFromXml(
                            note_id,
                            note_name,
                            note_text,
                            note_created,
                            note_lastChange
                        );
                        break;
                    case "style":
                        Style.insertFromXml(
                            style_id,
                            style_noteId,
                            style_begin,
                            style_end,
                            style_type,
                            style_value
                        );
                        break;
                    case "image":
                        Image.insertFromXml(
                                image_id,
                                image_noteId,
                                image_filename,
                                image_mime,
                                image_thumbnail,
                                image_image,
                                image_isAttachment,
                                image_textPos,
                                context
                        );
                        break;
                }
            }

            event = parser.next();
        }
    }
}
