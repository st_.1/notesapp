package cz.st.notesapp;


import androidx.appcompat.app.AppCompatActivity;
import cz.st.notesapp.Domain.*;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.BitSet;

public class ImageViewActivity extends AppCompatActivity {
    ImageView imageView;
    TextView imageTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        imageView = findViewById(R.id.imageView);
        imageTitleTextView = findViewById(R.id.imageTitleTextView);

        Intent intent = getIntent();
        long id = intent.getLongExtra("id", -1);
        String filename = intent.getStringExtra("filename");
        Uri uri = intent.getParcelableExtra("uri");

        if (id > 0) {
            // get bitmap from db
            Image image = Image.find(id);
            Bitmap imageBitmap = image.getImage(this);
            imageView.setImageBitmap(imageBitmap);
        } else {
            // get bitmap from filesystem
            try {
                Bitmap imageBitmap = ImageUtils.loadImageWithRotation(this, uri);
                imageView.setImageBitmap(imageBitmap);
            } catch (IOException e) {
                Toast.makeText(this, "Failed to load image", Toast.LENGTH_SHORT).show();
            }
        }

        imageTitleTextView.setText(filename);
    }
}